function [controller] = DynFlatness()
%DYNFLATNESS Summary of this function goes here
%
% Syntax: DYNFLATNESS
%
% Inputs:
%
% Outputs:
%
% See also: ---

% Authors: Daniel He� (Original Author), Davide Calzolari
% Date: 2016/09/19
% Revision: 0.1
% Technische Universitaet Muenchen 2016

controller.name='FLAT';
controller.control_point='CO';
controller.init=@(model,options)init(controller,model,options);
end

function controller = init(controller,model,options)
%get model dimension
n=length(model(0,zeros(10,1),zeros(5,1),options.p));
%initialize controller
switch func2str(model)
    case options.types{1}
        mapFCInput=@ForceController_pacejka;
end
controller.compute_input=@(varargin)compute_input(mapFCInput,n,varargin);
end

function [u] = compute_input(mapFCInput,n,varargin)

%reference trajectory
[x,tau,t,p] = parseInput(varargin{:});

%extract state vector
X=x(1);
Y=x(2);
psi=x(3);
vx=x(4);
vy=x(5);
omega=x(6);

% trajectory values
Xd = tau.X(t); Yd = tau.Y(t); 
theta = tau.theta(t); dtheta = tau.dtheta(t); 
ddtheta = tau.ddtheta(t); %dddtheta = tau.dddtheta(t);
v = tau.v(t); dv = tau.a(t);

%load param
m=p.m;
l_F=p.l_F;
l_R=p.l_R;
L=p.L;
mu0=p.mu0;
B_R=p.B_R;
C_R=p.C_R;
J=p.J;
h=p.h;
R=p.R;
g=9.81;

%   Reference:
%   [5] D. He�, M. Althoff, and T. Sattel, �Comparison of trajectory tracking
%   controllers for emergency situations,� in Intelligent Vehicles Symposium
%   (IV). IEEE, 2013, pp. 163�170.

%control point
lambda=p.J/p.m/p.l_R;

balance = 1;
K0=p.kA0;
K1=p.kA1;

%% decoupling point
% rotation matrices
ct = cos(theta); st = sin(theta);
cb = cos(theta-psi); sb = sin(theta-psi);
Rti = [ct,st;-st,ct];
Rb = [cb,-sb;sb,cb];
Rbi = [cb,sb;-sb,cb];

% calculate position of reference point
Xp = X + lambda * cos(psi);
Yp = Y + lambda * sin(psi);

% calculate velocity of reference point
hd(1,1) = vx;
hd(2,1) = vy + lambda * omega;

%% 1) position level: path tracking error: hp,dhp
hp = Rti * [Xp-Xd;Yp-Yd];
dhp = - dtheta * [-hp(2);hp(1)] ...
          - [v;0] + Rbi * hd;
      
%% 2) linear feedback: ddhp
ddhp = -K0 * hp - K1 * dhp;
      
%% 3) required acceleration dhd
dhd = Rb * ( ...
             ddhp + [dv;0] + ddtheta*[-hp(2);hp(1)] ...
                  + dtheta*[-dhp(2);dhp(1)]  ...
           )...
    - (omega-dtheta)*[-hd(2);hd(1)];
%% 4) compute the (required) wheel forces

% required traction/breaking force
Fb = ( dhd(1) - vy * omega ) * m;
Fxf = balance * Fb;
Fxr = (1-balance) * Fb;

% normal forces
fzf_max = (m*g*l_R)/(-mu0*h+L);  %the maximum normal force if all normal force is utilized for breaking
Fzf = max(0,min(fzf_max,l_R/L * m*g - h/L * Fb));
Fzr = m*g - Fzf;

% maximal absolute force
Fmaxf = mu0 * Fzf;
Fmaxr = mu0 * Fzr;

%non commanded
omega_R = vx/R;

% velocity at rear axle
vr = [vx; vy - l_R * omega];
% velocity at front axle
vf = [ vx; vy + l_F * omega ];
vf_abs = norm(vf);
fr = @(omega_R)-mu0*Fmaxr*sin(C_R*atan(B_R*norm((vr-[R*omega_R;0])./norm(vr))/mu0))...
                        .*(vr-[R*omega_R;0])./norm(vr)/norm((vr-[R*omega_R;0])./norm(vr));
                    
if norm((vr-[R*omega_R;0])./norm(vr))==0  
    Fyr = 0;
else
    Fyr = [0,1] * fr(omega_R);
end

%front lateral tire force
Fyf = (dhd(2) - (1/m-l_R*lambda/J) * Fyr + vx * omega) ...
    / (1/m + l_F*lambda/J);

% absolute front tire force
Ff_abs = sqrt(Fxf*Fxf+Fyf*Fyf);

% maximal tyre force exceeded?
if Ff_abs>Fmaxf
%     disp(['mu=' num2str(Ff_abs/Fmaxf)]);
    Fxf = Fxf * Fmaxf / Ff_abs;
    Fyf = Fyf * Fmaxf / Ff_abs;
    Ff_abs = Fmaxf;
end

ud = [Fxf;Fyf];

% the output of the controller
u= mapFCInput(ud,x,Ff_abs,Fmaxf,p);
u(end+1)=0;
end