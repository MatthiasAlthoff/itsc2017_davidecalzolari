
function [controller] = DynPassiveBased()
%DynPassiveBased Summary of this function goes here
%
% Syntax: DynPassiveBased
%
% Inputs:
%
% Outputs:
%
% See also: ---

% Author: Davide Calzolari
% Date: 2016/09/16
% Revision: 0.1
% Technische Universitaet Muenchen 2016

controller.name='PBPI';
controller.control_point='COM';
controller.init=@(model,options)init(controller,model,options);
end

function controller = init(controller,model,options)
%get model dimension
n=length(model(0,zeros(10,1),zeros(5,1),options.p));
%initialize controller
switch func2str(model)
    case options.types{1}
        mapInput=@AXController_pacejka;
end
controller.compute_input=@(varargin)compute_input(mapInput,n,varargin);
end

function [u] = compute_input(mapInput,n,varargin)

%reference trajectory
[state,tau,t,p] = parseInput(varargin{:});

%extract state vector
x=state(1);
y=state(2);
psi=state(3);
vx=state(4);
vy=state(5);
dpsi=state(6);
INT_z=state(n);

% trajectory values
pd = [tau.X(t);tau.Y(t)];
vd = [tau.dX(t);tau.dY(t)]; 
psid = tau.theta(t); dpsid = tau.dtheta(t);
lvd = tau.v(t); lad = tau.a(t); 

%   Reference:
%   [14] G. Tagne, R. Talj, and A. Charara, �Design and comparison of robust
%   nonlinear controllers for the lateral dynamics of intelligent vehicles�
%   IEEE Transactions on Intelligent Transportation Systems, vol. 17, no. 3,
%   pp. 796�809, 2016.

%error vector wrt desired vehicle configuration
[e,de]=getLocalError(state,pd,vd,psid,dpsid,0);

%gains   p. 805
KP=0.075;
KI=0.015;
lamba=1;

%   The tire friction coefficients cf and cr are already scaled by the
%   road friction coefficient mu0

%non linear gain KE
emax=0.25;
k0=12;
esat=saturate(e(2),-emax,+emax);    %[eq. 57]
KE=cosh(k0*esat);                   %[eq. 56]

%variable
z=de(2)+lamba*e(2);         %[eq. 54]

%desired curvature
rho=dpsid/lvd;
%control laws
dv=PID_AX(lad,e,de);
%Passive Based law          %[eq. 58]
steer=-KI*KE*INT_z-KP*KE*z ...
    + (p.l_F+p.l_R)*rho + ...
    (p.m*vx^2*(p.l_R*p.cr-p.l_F*p.cf))/(p.cf*p.cr*(p.l_R+p.l_F))*rho;

%steering angle saturation
steer=saturate( steer,-p.MAX_delta,p.MAX_delta);

% the output of the controller
u=mapInput(dv,state,steer,p);
u(end+1)=z;
end
