function TEST_RRT()
%TEST_RRT Summary of this function goes here
%
% Syntax: TEST_RRT
%
% Inputs:
%
% Outputs:
%
% See also: ---

% Author: Davide Calzolari
% Date: 2016/10/16 20:57:26
% Revision: 0.1
% Technische Universit�t M�nchen
close all hidden
savepath=[fileparts(which('TEST_RRT')) '\RRT_aggregated_results\'];

options=    default_options;
%setup
controllers = {
    KinLinearization
    KinSliding
    DynTwisting
    DynImmersion
    DynPassiveBased
    DynFlatness
    DynLQR
    DynBackstepping
    };
scenarios =     {'01_single_lane_change'};

%preload static trajectories
disp('preloading trajectories..')
taus=cell(length(scenarios),2);
for j=1:length(scenarios)
    taus{j,1}=Trajectory();
    taus{j,1}.load(['Scenarios/' scenarios{j}]);
    taus{j,1}.solveID(options.p);
    taus{j,2} = taus{j,1};
    taus{j,2}=taus{j,2}.transform(options.p.J/options.p.l_R/options.p.m).make_static(100);
    taus{j,3} = taus{j,1};
    taus{j,3} = taus{j,3}.transform(-options.p.l_R).make_static(100);
end
%bar
hwb = waitbar(0,'TEST RRT');
step = 0;
steps = length(scenarios)*length(controllers);

%run tests
for j=1:length(scenarios)
    %scenario level
    tau=taus{j,1};
    scenario=scenarios{j};
    
    for i=1:length(controllers)
        %collect
        controller= controllers{i};
        
        if strcmp(controller.control_point,'CO')
            tauD=taus{j,2};
        else if strcmp(controller.control_point,'REAR')
                tauD=taus{j,3};
            else
                tauD=taus{j,1};
            end
        end
        
        test = 'TEST06_RRT';
        p=options.p;
        controller=controller.init(@vmodel_A,options);
        model=@(t,x,xi_dx,xi_m,xi_p,tauD,p)modelRRT(t,x,xi_dx,xi_m,xi_p,controller.compute_input,tauD,p);
        TEST06_RRT(model,p,7,tau,tauD,[savepath '\' test '_' scenario '_' controller.name '.mat'])
        %RRT
        
        step = step + 1; waitbar(step/steps,hwb);
    end
end

delete(hwb);
end

function [dx] = modelRRT(t,x,xi_dx,xi_m,xi_p,fcontrol,tauD,p)
% t - time
% x - state
% xi_dx - process disturbance
% xi_m - measurement error
% xi_p - parameter mismatch

[u] = fcontrol(x+xi_m(t),tauD,t,p);
[dx] = vmodel_A(t,x,u,p);
dx = dx + xi_dx(t);
end