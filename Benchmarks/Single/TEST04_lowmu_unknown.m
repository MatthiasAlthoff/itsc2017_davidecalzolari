function [ benchmark ] = TEST04_lowmu_unknown(tau,tauD,controller,model,options,filename)
%TEST04 with unknown low mu

%initial state
x0=getVehicleX0(model,tau,options);
%low friction
options.p.mu0=0.6;
options.pc.mu0=1.0;
options.p.cf=options.p.cf*options.p.mu0;
options.p.cr=options.p.cr*options.p.mu0;
options.pc.cf=options.pc.cf*options.pc.mu0;
options.pc.cr=options.pc.cr*options.pc.mu0;

%initialize controller;
controller=controller.init(model,options);

%run
benchmark=simulate(tau,tauD{1},controller,model,x0,options);
vplot(benchmark,filename);
end

