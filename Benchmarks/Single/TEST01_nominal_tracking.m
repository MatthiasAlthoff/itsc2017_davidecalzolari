function [ benchmark ] = TEST01_nominal_tracking(tau,tauD,controller,model,options,filename)
%TEST nominal

%initial state
x0=getVehicleX0(model,tau,options);

%initialize controller;
controller=controller.init(model,options);

%run
benchmark=simulate(tau,tauD{1},controller,model,x0,options);
vplot(benchmark,filename);
end

