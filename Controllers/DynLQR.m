function [controller] = DynLQR()
%DynLQR Summary of this function goes here
%
% Syntax: DynLQR
%
% Inputs:
%
% Outputs:
%
% See also: ---

% Author: Davide Calzolari
% Date: 2016/09/16
% Revision: 0.1
% Technische Universitaet Muenchen 2016

controller.name='LQR';
controller.control_point='COM';
controller.init=@(model,options)init(controller,model,options);
end


function controller = init(controller,model,options)
%   Reference:
%   [20] J. M. Snider, “Automatic steering methods for autonomous automobile
%   path tracking,” Robotics Institute, Pittsburgh, PA, Tech. Rep. CMU-RITR-
%   09-08, 2009.

%get model dimension
n=length(model(0,zeros(10,1),zeros(5,1),options.p));
%initialize controller
switch func2str(model)
    case options.types{1}
        mapInput=@AXController_pacejka;
end

p=options.pc;
fname=['preload/Kopt_lqr_mu_' num2str(p.mu0) '.mat'];
%calculate optimal gains
if ~exist(fname,'file')
    %[eq. 29]
    Av=@(vx)[0,   1,  0,  0
            0, -(p.cf+p.cr)/(p.m*vx), (p.cf+p.cr)/p.m, -(p.l_F*p.cf-p.l_R*p.cr)/(p.m*vx)
            0,   0,  0,  1
            0, -(p.l_F*p.cf-p.l_R*p.cr)/(p.J*vx), (p.l_F*p.cf-p.l_R*p.cr)/p.J, -(p.l_F^2*p.cf+p.l_R^2*p.cr)/(p.J*vx)];
    Bv=[0 p.cf/p.m 0 p.l_F*p.cf/p.J]';
    Q=diag([0.075 0 0 0]); R=1*eye(1); N=zeros(4,1); %p. 
    for vx=15:1:22
        K(vx,1:4)=lqr(Av(vx),Bv,Q,R,N);
    end
    Kopt=@(vx)interp1(K,vx);
    save(fname,'Kopt');
else
    load(fname,'Kopt');
end

controller.compute_input=@(varargin)compute_input(mapInput,n,Kopt,varargin);
end

function [u] = compute_input(mapInput,n,Kopt,varargin)

%reference trajectory
[state,tau,t,p] = parseInput(varargin{:});

%extract state vector
x=state(1);
y=state(2);
psi=state(3);
vx=state(4);
vy=state(5);
omega=state(6);

% trajectory values
pd = [tau.X(t);tau.Y(t)];
vd = [tau.dX(t);tau.dY(t)];
lvd = tau.v(t);
lad = tau.a(t);
thetad = tau.theta(t);
omegad = tau.dtheta(t);

%   Reference:
%   [20] J. M. Snider, “Automatic steering methods for autonomous automobile
%   path tracking,” Robotics Institute, Pittsburgh, PA, Tech. Rep. CMU-RITR-
%   09-08, 2009.

%   The tire friction coefficients cf and cr represent total friction on
%   the front and rear tire respectively i.e. cf = 2Cf and cr = 2Cr

%error vector wrt desired vehicle configuration
[e,de]=getLocalError(state,pd,vd,thetad,omegad,0);

%error vector
z=[e(2),de(2),e(3),de(3)]';
%retrieve gain
K=Kopt(vx);
%feed forward term      %[page 43]
k=omegad/lvd;
Kv=(p.l_R*p.m)/(p.cf*p.L)-(p.l_F*p.m)/(p.cr*p.L);
k3=K(3);
delta_ff = p.L*k + Kv*(vx^2*k) - k3*(p.l_R*k-(p.l_F/p.cr)*(p.m*vx^2*k)/p.L);

%optimal feedback
steer = -K*z + delta_ff;          %[page 42]
%PID for longitudinal tracking
dv=PID_AX(lad,e,de);

%steering angle saturation
steer=saturate(steer,-p.MAX_delta,+p.MAX_delta);

% the output of the controller
u=mapInput(dv,state,steer,p);
u(end+1)=0;
end
