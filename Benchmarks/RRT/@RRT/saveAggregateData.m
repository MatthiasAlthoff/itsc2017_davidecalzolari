function saveAggregateData(files,filename)

data = struct();
for fi = 1:length(files)
    load(files{fi});
    %% Regroup for deviation plot
    SA = zeros(size(obj.states{1},1),1);
    ST = 0;
    for i = 2:length(obj.states);
        si = obj.states{i};
        ti = (i-1)*obj.dt;
        Rt = [cos(obj.tau.theta(ti)),sin(obj.tau.theta(ti));...
              -sin(obj.tau.theta(ti)),cos(obj.tau.theta(ti))];
        p = [obj.tau.X(ti);obj.tau.Y(ti)];
        for k = 1:size(si,2);
            eps_tn = Rt * (si(1:2,k)-p);
            si(1:2,k) = eps_tn;
        end
        SA = [SA,max(abs(si),[],2)]; %#ok<AGROW>
        ST = [ST,ti]; %#ok<AGROW>
    end
    data.(['T' num2str(fi)]) = ST;
    data.(['EpsilonT' num2str(fi)]) = SA(1,:);
    data.(['EpsilonN' num2str(fi)]) = SA(2,:);
    
    %% plot errors
    figure(1);hold all;box on;set(gcf(),'Name',filename);
    plot(ST,SA(1,:));
    plot(ST,SA(2,:));
    legend('\epsilon_t','\epsilon_n');
    ylabel('max(\epsilon) (m)');
    xlabel('t (s)');
end
save_struct_dat(data,filename);

function save_struct_dat(data,filename)
names = fieldnames(data);
delimiter1 = '\t';
delimiter2 = ',';
casenames = {};
for j = 1:length(data.(names{1}));casenames{j} = '';end
matrix = zeros(length(data.(names{1})),length(names));
for i = 1:length(names);
    matrix(:,i) = data.(names{i});
end
tblwrite(matrix,names,casenames,filename,delimiter1);
tblwrite(matrix,names,casenames,[filename '.csv'],delimiter2);
