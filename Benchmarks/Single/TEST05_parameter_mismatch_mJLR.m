function [ benchmark ] = TEST05_parameter_mismatch_mJLR(tau,tauD,controller,model,options,filename)
%TEST05 with mismatch parameters

%initial state
x0=getVehicleX0(model,tau,options);
%low friction
options.p.m=1.3*options.p.m;
options.p.J=1.3*options.p.J;
options.p.l_R=1.3*options.p.l_R;
options.p.L=options.p.l_R+options.p.l_F;

%initialize controller;
controller=controller.init(model,options);

%run
benchmark=simulate(tau,tauD{1},controller,model,x0,options);
vplot(benchmark,filename);
end

