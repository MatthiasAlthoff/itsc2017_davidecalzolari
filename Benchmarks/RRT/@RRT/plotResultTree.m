function plotResultTree(obj)
    
figure(1);hold all;box on;set(gcf(),'Name',obj.filename);
% title('Reachable CG-Positions');
tsample = 0:obj.dt:obj.tau.T;
xlabel('X(m)');ylabel('Y(m)');
x=fwrap(obj.tau.X,tsample);
y=fwrap(obj.tau.Y,tsample);
plot(x,y,'Color','red');

for i = 2:length(obj.states)
        SH = obj.states{i-1};
        SI = obj.states{i};
        BI = obj.backlinks{i};
        for k = 1:length(BI)
            plot([SI(1,k),SH(1,BI(k))],[SI(2,k),SH(2,BI(k))],'Marker','none','Color','blue');
        end
        if mod(i,5)==0
            drawnow;
        end
end