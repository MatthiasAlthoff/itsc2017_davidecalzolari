%% MC and RRT plots sd max dev

clear
close all
%MC
load TEST_MC_2017.mat

nc=size(b_test{1},1);
bs=[b_test{1}{:,1}];
bsn={b_test{1}{1,2};b_test{1}{2,2};b_test{1}{3,2};b_test{1}{4,2};b_test{1}{5,2};b_test{1}{6,2};b_test{1}{7,2};b_test{1}{8,2}};

nsamples=size(bs(1).errors,1)-1;
STD_=@(TheMatrix)arrayfun(@(idx) std(TheMatrix(idx,:)), 1:size(TheMatrix,1));
colors=get_colors;
figure('units','normalized','position',[.3 .2 .3 .3])
for ic=1:nc
    for k=1:nsamples
        e_t(k,:)=bs(ic).errors(k,1:2:end);
        e_n(k,:)=bs(ic).errors(k,2:2:end);
    end
    std_t=STD_(e_t);
    std_n=STD_(e_n);
    subplot(2,1,1)
    grid on
    ylabel([char(963) '(' char(949) '^{CG}_t) (m)']);
    plot((1:nsamples)*0.01,std_t,'-','Color',colors(ic,:),'LineWidth',1.2);
    hold on
    subplot(2,1,2)
    grid on
    ylabel([char(963) '(' char(949) '^{CG}_n) (m)']);
    plot((1:nsamples)*0.01,std_n,'-','Color',colors(ic,:),'LineWidth',1.2);
    xlabel('t (s)');
    hold on
    drawnow
    legend_info{ic}=bsn{ic}; %#ok<SAGROW>
    grid on
end
rect = [0.5, .97, .0, .0];
legend(legend_info,'Location',rect,'Orientation','Horizontal','EdgeColor','w')
%removeWhiteSpace(gca)
hold off

%% RRT
file_to_load = {
    'TEST06_RRT_01_single_lane_change_KINIO'
    'TEST06_RRT_01_single_lane_change_KINSM'
    'TEST06_RRT_01_single_lane_change_HOSM'
    'TEST06_RRT_01_single_lane_change_IandI'
    'TEST06_RRT_01_single_lane_change_PBPI'
    'TEST06_RRT_01_single_lane_change_FLAT'
    'TEST06_RRT_01_single_lane_change_LQR'
    'TEST06_RRT_01_single_lane_change_BKST'
};

figure('units','normalized','position',[.3 .2 .3 .3])
hold on
for ic=1:nc
    load(file_to_load{ic})
    for k=2:obj.i_finished-1
        SA=obj.states{k+1};
        p=SA(1:2,:)';
        pd=[obj.tau.X((k-1)*obj.dt), obj.tau.Y((k-1)*obj.dt)];
        theta=obj.tau.theta((k-1)*obj.dt);
        Rti=[cos(theta) sin(theta);-sin(theta) cos(theta)];
        e0=p-repmat(pd,obj.samples_per_time,1);
        for s=1:obj.samples_per_time
            etn(s,:)=(Rti'*e0(s,:)')';
        end
        max_e_t(k-1)=max(abs(etn(:,1)));
        max_e_n(k-1)=max(abs(etn(:,2)));
    end
    max_e_tn(ic,1) = norm([max(max_e_t), max(max_e_n)]);
    subplot(2,1,1)
    grid on
    plot((1:nsamples)*0.01,max_e_t,'-','Color',colors(ic,:),'LineWidth',1.2)
    ylabel(['max|' char(949) '^{CG}_t| (m)']);
    hold on
    subplot(2,1,2)
    grid on
    plot((1:nsamples)*0.01,max_e_n,'-','Color',colors(ic,:),'LineWidth',1.2)
    ylabel(['max|' char(949) '^{CG}_n| (m)']);
    xlabel('t (s)');
    hold on
    drawnow
end
legend(legend_info,'Location',rect,'Orientation','Horizontal','EdgeColor','w')
%removeWhiteSpace(gca)
hold off

%scoring
norm_max_e_tn = max_e_tn - min(max_e_tn(:));
norm_max_e_tn = norm_max_e_tn ./ max(norm_max_e_tn(:));
%scores
norm_max_e_tn=(1-norm_max_e_tn);
%disp(norm_max_e_tn)

%ref best
best_max_e_tn= min(max_e_tn(:));
ref_best_max_e_tn=max_e_tn./best_max_e_tn;
disp(ref_best_max_e_tn)