function [dv] = PID_AX(ff,e,de)
%PID_AX Summary of this function goes here
% 
% Syntax: PID_AX 
% 
% Inputs: 
% 
% Outputs: 
% 
% See also: ---

% Author: Davide Calzolari 
% Date: 2016/10/15 12:28:16 
% Revision: 0.1 
% Technische Universitaet Muenchen 2016

dv=ff-e(1)*5-de(1)*5;

end
