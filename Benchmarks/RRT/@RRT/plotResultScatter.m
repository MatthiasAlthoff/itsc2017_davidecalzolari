function plotResultScatter(obj)
SA = []; %zeros(size(states{2},1),size(states{2},2)*length(states));
ST = [];
for i = 1:length(obj.states);
    SA = [SA,obj.states{i}]; %#ok<AGROW>
    ST = [ST,ones(1,size(obj.states{i},2))*(i-1)*obj.dt]; %#ok<AGROW>
end
    
figure(1);hold all;box on;set(gcf(),'Name',obj.filename);
xlabel('X(m)');ylabel('Y(m)');
%title('Reachable CG-Positions');
tsample = 0:obj.dt:obj.tau.T;
x=fwrap(obj.tau.X,tsample);
y=fwrap(obj.tau.Y,tsample);
h(1) = plot(x,y,'Color','red');
h(1) = plot(SA(1,:),SA(2,:),'Marker','.','MarkerSize',1,'Color','blue','LineStyle','none');    

legend(h,'\tau','[X^{CG},Y^{CG}]');