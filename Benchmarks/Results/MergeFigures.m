clear
close all

names = {'KINIO','KINSM','HOSM','IandI','PBPI','FLAT','LQR','BKST'};

for name_ = names
    name=char(name_);
    load(['TEST06_RRT_01_single_lane_change_' name]);
    obj.plotResultScatter
    savepath='.\Benchmarks\Results\RRTMC_500\';
    legend off
    savefig([savepath '\TEST06_RRT_01_single_lane_change_' name]);
    close all
    f1=open(['TEST06_RRT_01_single_lane_change_' name '.fig']);
    xlim([0 42])
    ylim([-0.25 4])
    grid on
    f2=open(['TEST06_MC_01_single_lane_change_' name '_.fig']);
    %xlim([0 42])
    %ylim([-0.25 4])
    ax1 = get(f1, 'Children');
    ax2 = get(f2, 'Children');
    ax2Children = get(ax2,'Children');
    copyobj(ax2Children, ax1);
    set(f1, 'Position', [500, 500, 160*3, 50*3]);
    savefig(f1,[savepath 'TEST06_RRTMC_01_single_lane_change_' name]);
    %removeWhiteSpace(gca)
    saveas(f1,[[savepath 'TEST06_RRTMC_01_single_lane_change_' name] '.eps'],'epsc')
    close all
end