function [controller] = KinLinearization()
%KinLinearization Summary of this function goes here
%
% Syntax: KinLinearization
%
% Inputs:
%
% Outputs:
%
% See also: ---

% Author: Davide Calzolari
% Date: 2016/09/16
% Revision: 0.1
% Technische Universitaet Muenchen 2016

controller.name='KINIO';
controller.control_point='CO';
controller.init=@(model,options)init(controller,model,options);
end


function controller = init(controller,model,options)
%get model dimension
n=length(model(0,zeros(10,1),zeros(5,1),options.p));
%initialize controller
switch func2str(model)
    case options.types{1}
        mapInput=@AXController_pacejka;
    case options.types{2}
        mapInput=@map0;
end
controller.compute_input=@(varargin)compute_input(mapInput,n,varargin);
end

function [u] = compute_input(mapInput,n,varargin)

%reference trajectory
[state,tau,t,p] = parseInput(varargin{:});

%extract state vector
x=state(1);
y=state(2);
psi=state(3);
vx=state(4);
vy=state(5);
omega=state(6);

% trajectory values
pd = [tau.X(t);tau.Y(t)];
vd = [tau.dX(t);tau.dY(t)];
ad = [tau.ddX(t);tau.ddY(t)];

%   Based on the flatness property of the kinematic bycicle model
%
%   Presented for unicycle in:
%   [19] B. Siciliano, L. Sciavicco, L. Villani, and G. Oriolo, Robotics. 
%   Springer London, 2009

steer=state(n);
%steering angle saturation
steer=saturate(steer,-p.MAX_delta,+p.MAX_delta);

%look ahead b     
b=p.J/p.l_R/p.m;
look=[x+(b)*cos(psi); y+(b)*sin(psi)];

%cartesian error
ep=pd-look;
Rot=[cos(psi) -sin(psi); sin(psi) cos(psi)];
vp=Rot*([vx;vy+b*omega]);
ev=vd-vp;

% inversion matrices
% b used for the kinematic derivation starts from rear tire.
bkin = b+p.l_R;
[invT,C]=invT_bicycle(bkin,steer,state,p);

%gains
Kp=20;
Kd=20;

%virtual inputs
u1=ad(1)+Kp*ep(1)+Kd*ev(1);
u2=ad(2)+Kp*ep(2)+Kd*ev(2);

%apply inversion to obtain inputs
kin=invT*([u1;u2]-C);
dv=kin(1);
dsteer=kin(2);

% the output of the controller
u=mapInput(dv,state,steer,p);
u(end+1)=dsteer;
end
