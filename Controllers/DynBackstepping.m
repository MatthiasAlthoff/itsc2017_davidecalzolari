function [controller] = DynBackstepping()
%DYNTWISTING Summary of this function goes here
%
% Syntax: DynBackstepping
%
% Inputs:
%
% Outputs:
%
% See also: ---

% Author: Davide Calzolari
% Date: 2016/10/10
% Revision: 0.1
% Technische Universitaet Muenchen 2016

controller.name='BKST';
controller.control_point='CO';
controller.init=@(model,options)init(controller,model,options);
end

function controller = init(controller,model,options)
%load types
%get model dimension
n=length(model(0,zeros(10,1),zeros(5,1),options.p));
%initialize controller
switch func2str(model)
    case options.types{1}
        mapInput=@AXController_pacejka;
end

controller.compute_input=@(varargin)compute_input(mapInput,n,varargin);
end

function [u] = compute_input(mapInput,n,varargin)

%reference trajectory
[state,tau,t,p] = parseInput(varargin{:});

%extract state vector
x=state(1);
y=state(2);
psi=state(3);
vx=state(4);
vy=state(5);
omega=state(6);

% trajectory values
pd = [tau.X(t);tau.Y(t)];
vd = [tau.dX(t);tau.dY(t)]; 
thetad = tau.theta(t); wd = tau.dtheta(t);
lvd= tau.v(t); lad = tau.a(t);

%	Reference:
%	[7] J. Guo, P. Hu, and R. Wang, “Nonlinear coordinated steering and braking
%   control of vision-based autonomous vehicles in emergency obstacle
%   avoidance,” IEEE Trans. Intell. Transport. Syst., pp. 1–11, 2016

%   The tire friction coefficients cf and cr are already scaled by the
%   road friction coefficient mu0

%   The tire friction coefficients cf and cr represent total friction on
%   the front and rear tire respectively i.e. cf = 2Cf and cr = 2Cr

%   Tire friction (f_R) and air resistance (cx, cy) are not considered

%look ahead distance
DL=p.J/p.m/p.l_R;

%error vector wrt desired vehicle configuration + look ahead DL
[e,de]=getLocalError(state,pd,vd,thetad,wd,DL);

%gains
lamba=[.1;.1]*1;
k1=10;
k2=1;
l1=10;
gamma=0;
zeta1=1;
beta=0;
eps1=1;

%dyn functions					%[eq. 3]
g0=p.cf*(vy+p.l_F*omega)/(p.m*vx);
g1=1;   %   the output of the controller is directly acceleration, 
        %   not the force on the brake, hence g1=1
g2=p.cf/p.m;
g3=p.cf*p.l_F/p.J;
f0=+vy*omega;
f1=-(p.cf+p.cr)*vy/(p.m*vx)-(vx+(p.cf*p.l_F-p.cr*p.l_R)/(p.m*vx))*omega;
f2=-(p.cf*p.l_F^2+p.cr*p.l_R^2)*omega/(p.J*vx) - (p.cf*p.l_F-p.cr*p.l_R)*vy/(p.J*vx);
%error variables	
s1=e(2);						%[eq. 19]
ds1=de(2);
alpha1=-k1*s1+vy;				%[eq. 22]
s2=vx*e(3)-omega*DL-alpha1;		%[eq. 23]
p1=e(1)+de(1); 					%[eq. 30]	%NOTE: e(1) is added to assure e(1)->0
sigma1=-f0+lad-l1*p1-(p1*gamma^2)/(2*zeta1);	%[eq. 37]
sigma2=-s1-f0*e(3)-de(3)*vx+f2*DL+f1-k1*ds1-k2*s2-(s1*beta^2)/(2*eps1);
%control laws 
u_ST=lamba.*[sgn(p1);sgn(s2)];	%[eq. 38]
u_EQ=[g1, g0; g1*e(3), g0*e(3)-g2-g3*DL]^-1 * [sigma1;sigma2]; %[eq. 36]
u_C=u_EQ+u_ST;
dv=u_C(1);
steer=-u_C(2);

%steering angle saturation
steer=saturate(steer,-p.MAX_delta,p.MAX_delta);

% the output of the controller
u=mapInput(dv,state,steer,p);
u(end+1)=0;
end