function x = sampleBoxUniform(mid,delta)
x = rand(size(mid))*2.*delta+mid-delta;