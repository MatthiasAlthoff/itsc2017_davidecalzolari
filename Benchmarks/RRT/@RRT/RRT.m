classdef RRT<handle
   properties   
        state_sample_distance  %hypercube around nominal trajectory, which is used for sampling
        distance_x_normalization %weights for distance measure
        sample_input_mat %list of possible input combinations
        samples_per_time %number of samples for one point in time
        
        t_end   %end time
        dt      %length in time of one edge in graph
        dtsub   %length in time of a subsegment of an edge
        
        f       %model/differential equation, @(t,x,xi_dx,xi_m,xi_p)
        x0      %initial state / root of tree
        tau     %nominal trajectory
        
        %% RRT result variables
        statesamples    %the samples that were used
        states          %explored states
        backlinks       %edges in graph
        t_finished      %time up to which RRT has progressed
        i_finished      %index up to which RRT has progressed
        
        filename        %specifies file to which this object is saved
        
   end
   methods 
       % constructor
       function obj = RRT()
           %sampling
            obj.state_sample_distance = [1;1;8*pi/180;5;5;8*pi/180;0];
            obj.distance_x_normalization = [0.1;0.1;5*pi/180;5;5;5*pi/180;inf];
            obj.samples_per_time = 500;

            %simulation
            obj.dt = 0.01;
            obj.dtsub = 0.01;            
            obj.t_finished = -obj.dt;
            obj.i_finished = 0;

            %possible inputs
            load_parameters_error;
            sample_input_sgn = obj.combinator(2,size(em_sigma,1),'p','r');
            sample_input_sgn = (sample_input_sgn - 1)*2-1;
            obj.sample_input_mat = em_sigma*ones(1,size(sample_input_sgn,1)) .* sample_input_sgn';

            %result sets
            obj.statesamples = {};
            obj.states = {};
            obj.backlinks = {};
       end
   end
   methods
       
      main_loop(obj);
      
      plotResultScatter(obj);
      plotResultTree(obj);

      function set(obj,f,x0,tau,filename)
          obj.f = f;
          obj.x0 = x0;
          obj.tau = tau;
          obj.t_finished = -obj.dt;
          obj.i_finished = 1;
          obj.t_end = tau.T;
          obj.states{1} = x0;
          obj.filename = filename;
      end
      
      function x = sample_state(obj,t,n)
              x = obj.sampleBoxUniform([obj.tau.X(t);obj.tau.Y(t);obj.tau.psi0(t);...
                                    cos(obj.tau.psi0(t))*obj.tau.v(t);...
                                    sin(obj.tau.psi0(t))*obj.tau.v(t);
                                    obj.tau.dpsi0(t);0], ...
                                        obj.state_sample_distance);
              x = x(1:n);
      end
          
   end
   methods (Static)
      x = sampleBoxUniform(mid,delta);
      [A] = combinator(N,K,s1,s2);
      saveAggregateData(files,filename);
   end
end


