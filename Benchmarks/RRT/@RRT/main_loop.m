function main_loop(obj)

% simoptions = odeset('RelTol',1e-6,'AbsTol',1e-3);
% odefun = @(f,t1,t2,x0)ode15s(f,[t1,t2],x0,simoptions);
mydtsub = obj.dtsub;
odefun = @(f,t1,t2,x0)myode4_fixed(f,t1:mydtsub:t2,x0);
n = length(obj.x0);
mydistance_x_normalization = obj.distance_x_normalization(1:n);
mysample_input_mat = obj.sample_input_mat;
myf = obj.f;

t_start = obj.t_finished+obj.dt;
i = obj.i_finished;

%% RRT-MAIN-LOOP
for t = t_start : obj.dt : obj.t_end;  %iterate over points of time
    i = i+1;
    
    states_i = zeros(n,obj.samples_per_time);
    states_h = obj.states{i-1};
    backlinks_i = zeros(obj.samples_per_time,1);
    
    sampleset = zeros(n,obj.samples_per_time);
    for k = 1:size(sampleset,2);sampleset(:,k)=obj.sample_state(t+obj.dt,n);end;
    
    ts = t;
    te = ts + obj.dt;
    
    tic();
    parfor k = 1 : obj.samples_per_time  %iterate over sample states
        % sample goal state:
        xs = sampleset(:,k);
        % compare:
        dist = states_h - xs * ones(1,size(states_h,2));
        dist = dist ./ (mydistance_x_normalization*ones(1,size(states_h,2)));
        vdist = vnorm(dist);
        [~,imin] = min(vdist);
        xo = states_h(:,imin);
        backlinks_i(k) = imin;
        % sample measurement inputs
        mindist_input_samples = inf;
        minxf = 0;
        minj = 0;
        minXode = 0;
        
        for j = 1:size(mysample_input_mat,2)  %iterate over possible inputs to reach sample state
            em = mysample_input_mat(:,j);
            [~,Xode] = odefun(@(t,x)myf(t,x,@(t)0,@(t)em,@(t)0),ts,te,xo); %%!!!!!!!!!!!
            xf = Xode(end,:)';
            xfdist = norm((xs-xf)./mydistance_x_normalization);
            if xfdist < mindist_input_samples
                minxf = xf;
                mindist_input_samples = xfdist;
                minj = j;
                minXode = Xode;
            end
        end
        states_i(:,k) = minxf;
    end
    toc();
    
    obj.backlinks{i} = backlinks_i;    
    obj.states{i} = states_i;           
    obj.statesamples{i} = sampleset;   

    %save date
    obj.t_finished = t;
    obj.i_finished = i;
    save(obj.filename,'obj');
    
    disp(['t=' num2str(t)]);
end