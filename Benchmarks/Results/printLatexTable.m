function printLatexTable(rows,cols,data)
% numeric values you want to tabulate:
% this field has to be an array or a MATLAB table
% in this example we use an array
input.data = data;

% Optional fields:

% Set column labels (use empty string for no label):
input.tableColLabels = cols;
% Set row labels (use empty string for no label):
input.tableRowLabels = rows;

% Switch transposing/pivoting your table:
input.transposeTable = 0;

% Determine whether input.dataFormat is applied column or row based:
input.dataFormatMode = 'column'; % use 'column' or 'row'. if not set 'colum' is used

% Formatting-string to set the precision of the table values:
% For using different formats in different rows use a cell array like
% {myFormatString1,numberOfValues1,myFormatString2,numberOfValues2, ... }
% where myFormatString_ are formatting-strings and numberOfValues_ are the
% number of table columns or rows that the preceding formatting-string applies.
% Please make sure the sum of numberOfValues_ matches the number of columns or
% rows in input.tableData!
%
input.dataFormat = {'%10.2e',length(cols)-2,'%.2f',2}; % three digits precision for first two columns, one digit for the last

% Define how NaN values in input.tableData should be printed in the LaTex table:
input.dataNanString = '-';

% Column alignment in Latex table ('l'=left-justified, 'c'=centered,'r'=right-justified):
input.tableColumnAlignment = 'c';

% Switch table borders on/off (borders are enabled by default):
input.tableBorders = 0;

% Uses booktabs basic formating rules ('1' = using booktabs, '0' = not using booktabs). 
% Note that this option requires the booktabs package being available in your LaTex. 
% Also, setting the booktabs option to '1' overwrites input.tableBorders if it exists.
% input.booktabs = 0;


% LaTex table caption:
input.tableCaption = 'Results';

% LaTex table label:
input.tableLabel = 'resultlabel';

% Switch to generate a complete LaTex document or just a table:
input.makeCompleteLatexDocument = 1;

% call latexTable:
latex = latexTable(input);