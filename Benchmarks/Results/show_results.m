%% LOAD MANUALLY THE TEST.MAT FILE THEN RUN
tab='        ';
nc=size(b_test{1},1);
tabs=repmat(tab,nc,1);
    
disp([tab '     ' 'max. deviation (m)' tab 'avrg. deviation (m)' tab 'final deviation (m)' '      avrg. tire sat.']);
disp([tab tab ' t' tab 'n' tab '        t' tab 'n' tab '         t' tab 'n' tab '      t' tab 'n']);
testname=test;

%b_test=bench{2};
%scenario j
%colors
colors=get_colors;

for j=1:1
    bs=[b_test{j}{:,1}];
    bdn=[];
    for i=1:nc
        bsn{i}=b_test{j}{i,2}; %#ok<SAGROW>
    end
    %bsn={b_test{j}{1,2};b_test{j}{2,2};b_test{j}{3,2};b_test{j}{4,2};b_test{j}{5,2};b_test{j}{6,2};b_test{j}{7,2};b_test{j}{8,2}};

    scenario=['Scenario ' num2str(j)];
    disp(' ')
    
    max=num2str(reshape([bs.max_error],2,nc)','%10.2e');
    avg=num2str(reshape([bs.avg_error],2,nc)','%10.2e');
    fin=num2str(reshape([bs.end_error],2,nc)','%10.2e');
    tire=num2str(reshape([bs.avg_musat],2,nc)',2);

    disp([testname ', ' scenario])
    disp([char(bsn) tabs max tabs avg tabs fin tabs tire]);
    disp(' ')
    
    %table
    data=[reshape([bs.max_error],2,nc)', reshape([bs.avg_error],2,nc)',reshape([bs.end_error],2,nc)',reshape([bs.avg_musat],2,nc)'];
    rows=bsn';
    %col={'max. deviation (m)','avrg. deviation (m)','final deviation (m)','avrg. tire sat.'};
    cols={'t','n','t','n','t','n','t','n'};
    printLatexTable(rows,cols,data)
    
    %print all trajectories in one figure
    close all
    figure('units','normalized','position',[.3 .2 .4 .5])
    h1=subplot(4,1,1:2);
    axis([0 71 -3 4.5])
    %TT=vplot_ref(bs(1));
    TT=bs(1).data.T;
    for ic=1:size(b_test{j},1)
        vplot_aggregate(bs(ic),colors(ic,:));
        legend_info{ic}=bsn{ic}; %#ok<SAGROW>
    end
    vplot_ref(bs(1));
    legend_info{9}='[X^{ref},Y^{ref}]';
    rect = [0.5, .97, .0, .0];
    legend(legend_info,'Location',rect,'Orientation','Horizontal','EdgeColor','w')
    grid on
    hold off
    h3=subplot(4,1,3);
    p = get(h3, 'pos');
    p(2) = p(2) - 0.035;
    set(h3, 'pos', p);
    hold on
    for ic=1:size(b_test{j},1)
        plot(TT,bs(ic).data.error(:,1),'-','Color',colors(ic,:),'LineWidth',1.2);
    end
    ylabel([char(949) '^{CG}_t (m)']);
    set(h3, 'XTickLabel', [])
    grid on
    hold off
    h4=subplot(4,1,4);
    hold on
    
    for ic=1:size(b_test{j},1)
        plot(TT,bs(ic).data.error(:,2),'-','Color',colors(ic,:),'LineWidth',1.2);
    end
    xlabel('t (s)');
    ylabel([char(949) '^{CG}_n (m)']);
    grid on
    fig = gcf;
    align_Ylabels(fig)
    
%     
%     %align_Ylabels(fig)
%     
%     removeWhiteSpace(h1);
%     removeWhiteSpace(h3);
%     removeWhiteSpace(h4);
%     
    fig.PaperPositionMode = 'auto';
    fig_pos = fig.PaperPosition;
    fig.PaperSize = [fig_pos(3) fig_pos(4)];
    
    hold off
end
clear max
%score system based on AVG
%norm 0-1
avg=reshape([bs.avg_error],2,nc)';
tn_avg=arrayfun(@(idx) norm(avg(idx,:)), 1:size(avg,1))';
best_avg=min(tn_avg(:));
norm_avg = tn_avg - min(tn_avg(:));
norm_avg = norm_avg ./ max(norm_avg(:));
%scores
norm_avg=(1-norm_avg);
ref_best_avg=tn_avg./best_avg;
disp(ref_best_avg)
