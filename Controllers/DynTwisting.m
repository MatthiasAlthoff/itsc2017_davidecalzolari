function [controller] = DynTwisting()
%DYNTWISTING Summary of this function goes here
%
% Syntax: DYNTWISTING
%
% Inputs: 
%
% Outputs: 
%
% See also: ---

% Author: Davide Calzolari
% Date: 2016/09/19
% Revision: 0.1
% Technische Universitaet Muenchen 2016

controller.name='HOSM';
controller.control_point='COM';
controller.init=@(model,options)init(controller,model,options);
end

function controller = init(controller,model,options)
%load types
%get model dimension
n=length(model(0,zeros(10,1),zeros(5,1),options.p));
%initialize controller
switch func2str(model)
    case options.types{1}
        mapInput=@AXController_pacejka;
end
controller.compute_input=@(varargin)compute_input(mapInput,n,varargin);
end

function [u] = compute_input(mapInput,n,varargin)

%reference trajectory
[state,tau,t,p] = parseInput(varargin{:});

%extract state vector
x=state(1);
y=state(2);
psi=state(3);
vx=state(4);
vy=state(5);
dpsi=state(6);
u2=state(n);

% trajectory values
pd = [tau.X(t);tau.Y(t)];
vd = [tau.dX(t);tau.dY(t)]; 
psid = tau.theta(t); dpsid = tau.dtheta(t);
lvd = tau.v(t); lad = tau.a(t); 

%   Reference: 
%   [14] G. Tagne, R. Talj, and A. Charara, �Design and comparison of robust
%   nonlinear controllers for the lateral dynamics of intelligent vehicles�
%   IEEE Transactions on Intelligent Transportation Systems, vol. 17, no. 3,
%   pp. 796�809, 2016.

%error vector wrt desired vehicle configuration
[e,de]=getLocalError(state,pd,vd,psid,dpsid,0);

%gains  %p.805
lamba=8;
alpha1=0.008;
alpha2=0.008;

%   The tire friction coefficients cf and cr are already scaled by the
%   road friction coefficient mu0

%sliding surface
s=de(2)+lamba*e(2);     %[eq. 10]

%instantneuos radius of curvature
rho=dpsid/lvd;
%sideslip angle
beta = atan(vy/vx);

%dyn functions  	%[eq.13]
phi1=-(p.cf+p.cr)/(p.m)*beta-(p.l_F*p.cf-p.l_R*p.cr)/(p.m*vx)*dpsi-vx^2*rho+lamba*de(2);
phi2=p.cf/p.m;
%control laws
dv=PID_AX(lad,e,de);
%sliding control
sgn_s=sgn(s);
u1=-alpha1*sqrt(abs(s))*sgn_s;	%[eq.15]
du2=-alpha2*sgn_s;                
steer_ST=u1+u2;                
steer_EQ=-(1/phi2)*phi1;        %[eq.17]
steer=steer_EQ+steer_ST;        %[eq.18]

%steering angle saturation
steer=saturate( steer,-p.MAX_delta,p.MAX_delta);

% the output of the controller
u=mapInput(dv,state,steer,p);
u(end+1)=du2;
end