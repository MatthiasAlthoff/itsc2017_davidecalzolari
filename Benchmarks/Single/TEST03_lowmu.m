function [ benchmark ] = TEST03_lowmu(tau,tauD,controller,model,options,filename)
%TEST03 with low mu

%initial state
x0=getVehicleX0(model,tau,options);
%low friction
options.p.mu0=0.6;
options.pc.mu0=0.6;
options.p.cf=options.p.cf*options.p.mu0;
options.p.cr=options.p.cr*options.p.mu0;
options.pc.cf=options.pc.cf*options.pc.mu0;
options.pc.cr=options.pc.cr*options.pc.mu0;

%initialize controller;
controller=controller.init(model,options);

%run                    %{2} because mu0 is known to the controller
benchmark=simulate(tau,tauD{2},controller,model,x0,options);
vplot(benchmark,filename);
end

