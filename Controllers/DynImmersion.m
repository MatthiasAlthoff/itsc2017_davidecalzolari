function [controller] = DynImmersion()
%DynImmersion Summary of this function goes here
%
% Syntax: DynImmersion
%
% Inputs:
%
% Outputs:
%
% See also: ---

% Author: Davide Calzolari
% Date: 2016/09/16
% Revision: 0.1
% Technische Universitaet Muenchen 2016

controller.name='IandI';
controller.control_point='COM';
controller.init=@(model,options)init(controller,model,options);
end

function controller = init(controller,model,options)
%get model dimension
n=length(model(0,zeros(10,1),zeros(5,1),options.p));
%initialize controller
switch func2str(model)
    case options.types{1}
        mapInput=@AXController_pacejka;
end
controller.compute_input=@(varargin)compute_input(mapInput,n,varargin);
end

function [u] = compute_input(mapInput,n,varargin)

%reference trajectory
[state,tau,t,p] = parseInput(varargin{:});

%extract state vector
x=state(1);
y=state(2);
psi=state(3);
vx=state(4);
vy=state(5);
dpsi=state(6);
INT_e=state(n);

% trajectory values
pd = [tau.X(t);tau.Y(t)];
vd = [tau.dX(t);tau.dY(t)]; 
psid = tau.theta(t); dpsid = tau.dtheta(t);
lvd = tau.v(t); lad = tau.a(t); 

%error vector wrt desired vehicle configuration
[e,de]=getLocalError(state,pd,vd,psid,dpsid,0);

%   Reference: 
%   [14] G. Tagne, R. Talj, and A. Charara, �Design and comparison of robust
%   nonlinear controllers for the lateral dynamics of intelligent vehicles�
%   IEEE Transactions on Intelligent Transportation Systems, vol. 17, no. 3,
%   pp. 796�809, 2016.

%gains  %p.805
lambda=8;
K1=2;
K2=0.5;

%   The tire friction coefficients cf and cr are already scaled by the
%   road friction coefficient mu0

%desired curvature
rho=dpsid/lvd;
%slip angle
beta=atan(vy/vx);
%control laws
dv=PID_AX(lad,e,de);
%I&I law                 % [eq. 30]
delta=-p.m*(K1+lambda)*de(2)/p.cf - p.m*(K1*lambda+K2)*e(2)/p.cf ...
    -p.m*K2*lambda/p.cf*INT_e +(p.cf+p.cr)/p.cf*beta + ...
    (p.l_F*p.cf-p.l_R*p.cr)/(p.cf*vx)*dpsi+p.m*vx^2/p.cf*rho;

%steering angle saturation
delta=saturate( delta,-p.MAX_delta,p.MAX_delta);

% the output of the controller
u=mapInput(dv,state,delta,p);
u(end+1)=e(2);
end
