function [T,X] = myode4_fixed(f,Tint,x0)
%integration mit klassischem  Runge-Kutta Verfahren, Ordnung 4
%Nur einen Schritt durchführen!!
X = [x0';zeros(length(Tint)-1,length(x0))];
T = Tint;

for i = 1:length(T)-1
    j = i+1;
    ti = T(i);
    tj = T(j);
    h = tj-ti;
    
    %bekannter Zustandspunkt zum Zeitpunkt ti
    xi = X(i,:)';
    
    %Steigungen
    k1 = f( ti, xi );
    k2 = f( ti + h/2, xi + h/2 * k1 );
    k3 = f( ti + h/2, xi + h/2 * k2 );
    k4 = f( ti + h, xi + h * k3 );    
    
    %R-K-4
    xj_4 = xi + h * (k1 + 2*k2 + 2*k3 + k4)/6;
    X(j,:) = xj_4';
end