function [ benchmark ] = TEST02_initial_step_ypsi(tau,tauD,controller,model,options,filename)
%TEST02 with initial deviation

%initial state
x0=getVehicleX0(model,tau,options);
x0=x0+[0 -0.2 degtorad(-3) 0 0 0 0]';

%initialize controller;
controller=controller.init(model,options);

%run
benchmark=simulate(tau,tauD{1},controller,model,x0,options);
vplot(benchmark,filename);
end

