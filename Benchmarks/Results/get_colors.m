function [ colors ] = get_colors( )
%GET_COLORS

colors=.8*[[0 0 1];[0 1 0];[0 1 1];[1 0 1];[1.2 .5 .4];[.5 .7 .5];[1 1 0];[0.2 0.2 0.2]];
end

