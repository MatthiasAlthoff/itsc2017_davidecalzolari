function TEST_SINGLE()
%TEST_ONE_SHOT Summary of this function goes here
%
% Syntax: TEST_ONE_SHOT
%
% Inputs:
%
% Outputs:
%
% See also: ---

% Author: Davide Calzolari 
% Date: 2016/10/16 20:57:26
% Revision: 0.1
% Technische Universit�t M�nchen 2016

close all
savepath='.\Benchmarks\Results';

model=      @vmodel_A;
options=    default_options;
%setup
controllers = {
    KinLinearization
    KinSliding
    DynTwisting
    DynImmersion
    DynPassiveBased
    DynFlatness
    DynLQR
    DynBackstepping
    };
tests = {
           @TEST01_nominal_tracking
          @TEST02_initial_step_ypsi
           @TEST03_lowmu
           @TEST04_lowmu_unknown
         @TEST05_parameter_mismatch_mJLR
    };
scenarios = {
   %'01_single_lane_change'
   '02_double_lane_change_comfort'
    };

%preload static trajectories
disp('preloading trajectories..')
taus=cell(length(scenarios),3,2);
for c=1:2   %cases: mu0 = 1 or 0.6
    for j=1:length(scenarios)
        if c==1
            options.p.mu0=1;
        end
        if c==2
            options.p.mu0=0.6;
        end
        taus{j,1,c}=Trajectory();
        taus{j,1,c}.load(['Scenarios/' scenarios{j}]);
        taus{j,1,c}.solveID(options.p);
        taus{j,2,c}=taus{j,1,c};
        taus{j,2,c} = taus{j,2,c}.transform(options.p.J/options.p.l_R/options.p.m).make_static(100);
        taus{j,3,c} = taus{j,1,c};
        taus{j,3,c} = taus{j,3,c}.transform(-options.p.l_R).make_static(100);
    end
end
%bar
hwb = waitbar(0,'TEST');
step = 0;
steps = length(tests)*length(scenarios)*length(controllers);

%run tests
bench=cell(length(tests),1);
for t=1:length(tests)
    %test level
    testfun=tests{t};
    test=func2str(testfun);
    
    for j=1:length(scenarios)
        %scenario level
        tau=taus{j,1};
        scenario=scenarios{j};
        
        for i=1:length(controllers)
            %controller
            controller= controllers{i};
            
            if strcmp(controller.control_point,'CO')
                tauD={taus{j,2,:}};
            else if strcmp(controller.control_point,'REAR')
                    tauD={taus{j,3,:}};
                else
                    tauD={taus{j,1,:}};
                end
            end
            %run
            config=[test ': ' controller.name ' on ' scenario];
            disp(config);
            options=default_options;
            b=testfun(tau,tauD,controller,model,options,[savepath '\figures\' test '_' scenario '_' controller.name '_']);
            %save
            bench{t,1}{j,1}{i,1}=b;
            bench{t,1}{j,1}{i,2}=controller.name;
            bench{t,1}{j,2}=scenario;
            bench{t,2}=test;
            step = step + 1; waitbar(step/steps,hwb);
        end
    end
    %save sigle tests
    b_test=bench{t,1};
    save([savepath '\' test '.mat'], 'b_test','test');
end
%save benchmarks
save('global_test1_6.mat', 'bench');

delete(hwb);
end