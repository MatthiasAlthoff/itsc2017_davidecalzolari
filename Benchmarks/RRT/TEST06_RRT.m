function result = TEST06_RRT(model,p,n,tau,tauD,filename)
% model - vehicle and controller to be tested (function)
%          [dx,y] = model(t,x,xi_dx,xi_m,xi_p,tauD,p)
%                 dx - state change
%                 y - output
%                 t - time
%                 x - state
%                 xi_dx - process disturbance
%                 xi_m - measurement error
%                 xi_p - parameter mismatch
% tauD - trajectory of reference point

%% define initial state
x0 = zeros(n,1);
x0(1)=tau.X(0);
x0(2)=tau.Y(0);
x0(3)=tau.theta(0);
x0(4)=tau.v(0);
x0(5)=0;
x0(6)=0;

rrt = RRT();
rrt.set(@(t,x,xi_dx,xi_m,xi_p)model(t,x,xi_dx,xi_m,xi_p,tauD,p),...
         x0, tau, filename);
rrt.main_loop();
end