function [dx,y] = vmodel_K(t,x,u,p)
%Bicycle Model with 
% state x=[X,Y,psi,vx,vy,omega]

%get state and parameters from x, p vectors
l_F=p.l_F;
l_R=p.l_R;
L=p.L;

%state
    %position of rear tire
    %X = x(1);
    %Y = x(2);
    psi = x(3);
    
    %velocity
    vx = x(4);
    %vy = x(5);
    %omega = x(6);
    
    dv=u(2);
    delta=u(1);
%kinematics only
    vr=norm([vx 0]);
    v0x = vr*cos(psi);
    v0y = vr*sin(psi);
    omega=vr/L*tan(delta);
    
    %cp = cos(psi);sp = sin(psi);
    domega = 0; %tan(delta)*dv/L + (vr*(tan(delta)^2 + 1)*u(3))/L;
    
%dx = zeros(size(x));
%position
    dx(1,1) = v0x;
    dx(2,1) = v0y;
    dx(3,1) = omega;
%velocity
    dx(4,1) = dv;
    dx(5,1) = 0;
    dx(6,1) = domega;
    dx(7,1) = u(3);
    
    ff=0;
    fr=0;
    Ff=0;
    Fr=0;
y = [ff;fr;delta;Ff;Fr];
