%% define range for measurement error
em_sigma = zeros(6,1);
em_sigma(1) = 0.05;       %X
em_sigma(2) = 0.05;       %Y
em_sigma(3) = 1/180*pi;   %psi
em_sigma(4) = 0.05;       %vx
em_sigma(5) = 0.05;       %vy
em_sigma(6) = 1/180*pi;   %omega
em_sigma(7) = 0;   %omega

%% parameter error
ep_int_rel = zeros(13,1);
ep_int_rel(1) = 0.15;       %m
ep_int_rel(2) = 0.1;        %J
ep_int_rel(3) = 0;          %l_f
ep_int_rel(4) = 0.02;       %l_r
ep_int_rel(5) = 0.05;       %h
ep_int_rel(6) = 0.1;        %mue0

%% disturbance
edx_sigma = zeros(6,1);
edx_sigma(4) = 0.1;         %d/dt vx
edx_sigma(5) = 0.1;         %d/dt vy
edx_sigma(6) = 0.1;         %d/dt omega
