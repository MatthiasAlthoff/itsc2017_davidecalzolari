function [ benchmark ] = TEST06_MC(tau,tauD,controller,model,options,filename)
%TEST nominal

%initial state
x0=getVehicleX0(model,tau,options);
options.stepSize=0.01;

%noise, a row input of sigmas
options.V=[5e-2 5e-2 1.75e-2 5e-2 5e-2 1.75e-2 0];

%initialize controller;
controller=controller.init(model,options);

%% run
runs=500;
parfor ii=1:runs
    b(ii)=simulate(tau,tauD,controller,model,x0,options);
end

%% Plot Results
tspan=b(1).data.T;
curve(1,:)=b(1).data.tau.X(tspan)';
curve(2,:)=b(1).data.tau.Y(tspan)';
DISPLAY_AXIS=[0 45 -.5 3.5];
close all
plot(curve(1,:),curve(2,:),'LineWidth',1,'Color','r');
axis(DISPLAY_AXIS);
hold on
for i=1:runs
    toplot=b(i).data.X;
    plot(toplot(:,1),toplot(:,2),'k');
end
box on;
xlabel('X (m)');
ylabel('Y (m)');
hold off
drawnow
myprint(filename)

%aggregate results
data=[b.data];
e=[data.error];
benchmark.errors=e;
benchmark.var_avg_ep=var(reshape([b.avg_error],2,runs)');
benchmark.var_end_ep=var(reshape([b.end_error],2,runs)');

end

function myprint(filename)
set(gcf,'PaperPositionMode','auto');
%saveas(gcf(),[filename '.fig']);
%print([filename '.png'],'-dpng','-r600');    
print([filename '.eps'],'-deps2','-r300');
savefig(filename);
end
